<?php
namespace Inmovsoftware\SendmdcApi\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Inmovsoftware\SendmdcApi\Models\V1\Sendmdc;
use Inmovsoftware\SendmdcApi\Http\Resources\V1\GlobalCollection;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class SendmdcController extends Controller
{
    public function index(Request $request)
    {
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";
        $sortField = "text";

        $item = News::orderBy($sortField, $sortOrder);
        if (!empty($filterValue)) {
            $item->where('it_categories_news_id', 'like', "%$filterValue%");
        }

        if (empty($pageSize)) {
            $pageSize = 10;
        }
        $item->with('Newscategory');
        $item->where('status', 'A');
        return new GlobalCollection($item->paginate($pageSize));
    }


    public function store(Request $request)
    {
        $data = $request->validate([
        "id_business_id" => "required|integer|exists:it_business,id",
        "title" => "required|max:255",
        "photo" => "nullable|file|max:5120|mimes:jpg,gif,jpeg,png",
        "it_categories_news_id" => "required|integer|exists:it_categories_news,id",
        "publication_date" => "required|max:50",
        "close_date" => "required|max:50",
        "status" => "nullable|in:A,C",
        "text" => "required"
    ]);

    if(!empty($data["photo"])){
        $new_name = time() . '.' . $request->photo->extension();
        $path = $request->photo->storeAs('news_images', $new_name, 'public');
        $data["photo"] = $path;
    }
    $data["creation_date"]= Carbon::now();
    $data = request()->only('id_business_id', 'title', 'photo', 'it_categories_news_id', 'creation_date', 'update_date', 'publication_date', 'close_date', 'status', 'text');
    $data = array_filter($data);


    $News = News::firstOrNew($data);
    return response()->json($News);
    }


    /**
     * Display the specified resource.
     *
     * @param  Inmovsoftware\SendmdcApi\Models\V1\Sendmdc $sendmdc
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {

        $news->Newscategory;
        return response()->json($news);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Inmovsoftware\SendmdcApi\Models\V1\Sendmdc $sendmdc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sendmdc $sendmdc)
    {
        $data = $request->validate([
            "it_business_id" => "required|integer|exists:it_business,id",
            "name" => "required|max:50",
            "status" => "nullable|in:A,C"
        ]);

        $company->update($data);
        return response()->json($company);



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Inmovsoftware\SendmdcApi\Models\V1\Sendmdc $sendmdc
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sendmdc $sendmdc)
    {
        $item = $position->delete();
        if ($item) {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('exceptions.element_not_found')]
                    ]
                ],
                401
            );
                    } else {
            return response()->json($item);
                    }
    }

}
