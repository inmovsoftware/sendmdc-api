<?php

namespace Inmovsoftware\SendmdcApi\Models\V1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sendmdc extends Model
{
    use SoftDeletes;
    protected $table = "it_news";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];
    protected $fillable = ['id_business_id', 'title', 'photo', 'it_categories_news_id', 'creation_date', 'update_date', 'publication_date', 'close_date', 'status', 'text'];


    public function Newscategory()
    {
        return $this->belongsTo('Inmovsoftware\NewsApi\Models\V1\Categories', 'it_categories_news_id', 'id');

    }

    public function scopefilterValue($query, $param)
    {
        $query->orwhere($this->table. ".text", 'like', "%$param%");
    }

    public static function scopethisUser(){
        $$query->it_users = auth()->user()->id;
    }



}
